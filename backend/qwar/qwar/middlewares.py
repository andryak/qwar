import logging
from urllib.parse import parse_qs

from channels.auth import UserLazyObject
from channels.db import database_sync_to_async
from channels.middleware import BaseMiddleware
from rest_framework.authtoken.models import Token

logger = logging.getLogger(__name__)


class AuthTokenMiddleware(BaseMiddleware):
    """
    Middleware which populates scope["user"] from a token provided in the query string.
    """

    def populate_scope(self, scope):
        if 'user' not in scope:
            scope['user'] = UserLazyObject()

    async def resolve_scope(self, scope):
        if 'query_string' not in scope:
            raise ValueError('AuthTokenMiddleware requires a query string.')

        logger.info('Extracting token from query string')
        query_string = scope['query_string']
        decoded_query_string = parse_qs(query_string.decode('utf8'))
        token, = decoded_query_string.get('token', [''])

        if not token:
            logger.info('Token is missing, aborting connection')
            raise ValueError('AuthTokenMiddleware requires a token.')

        try:
            token = await self.get_token_by_key(token)
        except Token.DoesNotExist:
            logger.info('Token is not valid, aborting connection')
            raise ValueError('Invalid token.')
        else:
            logger.info('Token is valid')

        user = await self.get_user_from_token(token)
        logger.info('Token corresponds to user %s', user.username)

        scope['user']._wrapped = user

    @database_sync_to_async
    def get_token_by_key(self, key):
        return Token.objects.get(key=key)

    @database_sync_to_async
    def get_user_from_token(self, token):
        return token.user
