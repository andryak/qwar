from channels.routing import ProtocolTypeRouter, URLRouter

from app.routing import websocket_urlpatterns
from qwar.middlewares import AuthTokenMiddleware

application = ProtocolTypeRouter({
    'websocket': AuthTokenMiddleware(
        URLRouter(
            websocket_urlpatterns
        ),
    ),
})
