from app import urls as app_urls
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    *app_urls.urlpatterns,
]
