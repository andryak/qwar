from django.http import JsonResponse, HttpResponse


class HttpResponseCreated(HttpResponse):
    status_code = 201


class JsonResponseCreated(JsonResponse):
    status_code = 201


class JsonResponseBadRequest(JsonResponse):
    status_code = 400


class JsonResponseNotFound(JsonResponse):
    status_code = 404
