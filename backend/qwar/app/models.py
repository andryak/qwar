from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models import CASCADE, F


class GameManager(models.Manager):
    def get_by_code(self, code):
        code_as_id = int(code, 16)
        return self.get(id=code_as_id)


class Game(models.Model):
    class Status(models.TextChoices):
        IDLE = 'IDLE'
        RUNNING = 'RUNNING'
        SETTLED = 'SETTLED'

    id = models.AutoField(primary_key=True)
    creator = models.ForeignKey(User, on_delete=CASCADE)
    status = models.CharField(max_length=10, choices=Status.choices, default=Status.IDLE)

    objects = GameManager()

    def get_code(self):
        return hex(self.id).lstrip('0x').zfill(4)

    def leaderboard(self):
        queryset = Player.objects.filter(game=self)\
            .annotate(username=F('user__username'))\
            .order_by('-kills', 'deaths')
        return [
            {
                'username': player_info.username,
                'deaths': player_info.deaths,
                'kills': player_info.kills,
            }
            for player_info in queryset
        ]

    def __str__(self):
        return self.get_code()


class Player(models.Model):
    user = models.ForeignKey(User, on_delete=CASCADE)
    game = models.ForeignKey(Game, on_delete=CASCADE)
    deaths = models.PositiveIntegerField(default=0, null=False)
    kills = models.PositiveIntegerField(default=0, null=False)

    class Meta:
        unique_together = [['user', 'game']]

    def __str__(self):
        return f'{self.user.username} in {self.game}'


class KillLog(models.Model):
    killer = models.ForeignKey(Player, on_delete=CASCADE, related_name='killer_logs')
    victim = models.ForeignKey(Player, on_delete=CASCADE, related_name='victim_logs')

    def clean(self):
        # Killer and victim must be different and in the same game.
        if self.killer == self.victim:
            raise ValidationError('Killer and victim cannot be the same player.')

        if self.killer.game != self.victim.game:
            raise ValidationError('Killer and victim must be in the same game.')

    def __str__(self):
        return f'{self.killer.user.username} killed {self.victim.user.username} in {self.killer.game}'
