from django.contrib import admin
from app.models import Game, Player, KillLog


class GameAdmin(admin.ModelAdmin):
    list_display = ('code', 'creator', 'status')

    def code(self, obj):
        return obj.get_code()


class PlayerAdmin(admin.ModelAdmin):
    list_display = ('user', 'game', 'deaths', 'kills')


class KillLogAdmin(admin.ModelAdmin):
    list_display = ('killer', 'victim', 'game')

    def game(self, obj):
        return obj.killer.game


admin.site.register(Game, GameAdmin)
admin.site.register(Player, PlayerAdmin)
admin.site.register(KillLog, KillLogAdmin)
