from django.contrib.auth.models import User
from django.db import IntegrityError
from django.http import JsonResponse
from django.utils.translation import gettext as _
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny

from app.models import Game, Player
from app.utils import JsonResponseBadRequest, JsonResponseNotFound, HttpResponseCreated


@api_view(['POST'])
@permission_classes([AllowAny])
def signup(request):
    username = request.data.get('username')
    password = request.data.get('password')

    if not username:
        error = _('Mandatory field.')
        return JsonResponseBadRequest({'username': [error]})

    if not password:
        error = _('Mandatory field.')
        return JsonResponseBadRequest({'password': [error]})

    try:
        user = User.objects.create_user(username=username, password=password)
    except IntegrityError:
        error = _('Username already exists.')
        return JsonResponseBadRequest({'username': [error]})

    token, created = Token.objects.get_or_create(user=user)
    return JsonResponse({'token': token.key})


@api_view(['POST'])
def create_game(request):
    game = Game.objects.create(creator=request.user)
    Player.objects.create(user=request.user, game=game)
    return JsonResponse({'code': game.get_code()})


@api_view(['POST'])
def create_player(request, game_code):
    try:
        game = Game.objects.get_by_code(game_code)
    except Game.DoesNotExist:
        error = _('Game not found.')
        return JsonResponseNotFound({'non_field_errors': [error]})

    if game.status != Game.Status.IDLE:
        error = _('You cannot join a game that has already started.')
        return JsonResponseBadRequest({'non_field_errors': [error]})

    try:
        Player.objects.create(user=request.user, game=game)
    except IntegrityError:
        error = _('You already joined this game.')
        return JsonResponseBadRequest({'non_field_errors': [error]})
    else:
        return HttpResponseCreated()


@api_view()
def leaderboard(request, game_code):
    try:
        game = Game.objects.get_by_code(game_code)
    except Game.DoesNotExist:
        error = _('Game not found.')
        return JsonResponseNotFound({'non_field_errors': [error]})

    return JsonResponse({'leaderboard': game.leaderboard()})
