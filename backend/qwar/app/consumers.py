import logging

from channels.db import database_sync_to_async
from channels.generic.websocket import AsyncJsonWebsocketConsumer

from app.models import Game, Player

logger = logging.getLogger(__name__)


class GameConsumer(AsyncJsonWebsocketConsumer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.username = None
        self.room_group_name = None

    @database_sync_to_async
    def get_game(self, game_code):
        return Game.objects.get_by_code(game_code)

    @database_sync_to_async
    def is_participating(self, user, game):
        return Player.objects.filter(user=user, game=game).exists()

    async def send_to_group(self, data):
        return await self.channel_layer.group_send(self.room_group_name, data)

    async def connect(self):
        user = self.scope['user']
        game_code = self.scope['url_route']['kwargs']['game_code']
        logger.info('Received connection request from user %s to game %s', user.username, game_code)

        # Check that the game exists.
        logger.info('Checking that game exists')
        try:
            game = await self.get_game(game_code)
        except Game.DoesNotExist:
            logger.info('Game does not exist, aborting connection')
            return await self.close()
        else:
            logger.info('Game exists')

        # Check that the game is still open.
        logger.info('Checking game status')
        if game.status == Game.Status.SETTLED:
            logger.info('Game is settled, aborting connection')
            return await self.close()
        else:
            logger.info('Game status is %s', game.status)

        # Check that the user is participating in this game.
        is_participating = await self.is_participating(user=user, game=game)
        if not is_participating:
            logger.info('This user is not participating in this game, aborting connection')
            return await self.close()

        # Join room group
        self.username = user.username
        self.room_group_name = 'game_%s' % game_code

        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )

        logger.info('Accepting connection from %s', user.username)
        await self.accept()

    async def disconnect(self, close_code):
        if not self.username:
            return

        logger.info('User %s disconnected', self.username)
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    # Receive message from WebSocket
    async def receive_json(self, content, **kwargs):
        event, data = content['event'], content['data']

        if event == 'message':
            await self.on_message(data)
        elif event == 'shoot':
            await self.on_shoot(data)
        elif event == 'activate_barrier':
            await self.on_activate_barrier()

    async def on_message(self, message):
        # Send message to room group
        logger.info('Got message `%s` from %s', message, self.username)
        await self.send_to_group({
            'type': 'chat_message',
            'message': message
        })

    async def on_shoot(self, target):
        logger.info('User `%s` shot `%s`', self.username, target)
        raise NotImplementedError

    async def on_activate_barrier(self):
        logger.info('User `%s` activates a barrier', self.username)
        raise NotImplementedError

    # Receive message from room group
    async def chat_message(self, event):
        message = event['message']

        # Send message to WebSocket
        await self.send_json({
            'event': 'message',
            'data': message
        })
