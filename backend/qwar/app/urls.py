from . import views
from django.urls import path, re_path
from rest_framework.authtoken import views as authtoken_views

urlpatterns = [
    path('login/', authtoken_views.obtain_auth_token),
    path('signup/', views.signup),
    path('game/', views.create_game),
    re_path(r'^game/(?P<game_code>[0-9a-fA-F]+)/$', views.leaderboard),
    re_path(r'^game/(?P<game_code>[0-9a-fA-F]+)/player/$', views.create_player),
]
