# Summary

TODO

# Prerequisites

This is a `Python 3.9` project. It leverages a `Redis` instance running in a `Docker` container to enable websocket communication.
Refer to https://docs.python.org/3.9/ and https://docs.docker.com/get-docker/ for installation.

# Install

The project ships with a `requirements.txt` that can be used to install dependencies.

Start by creating a project-level virtual environment named `venv`.
```
python -m venv venv
```

Activate the virtual environment, upgrade the version of `pip` if necessary, and install project dependencies.
```
source venv/bin/activate
pip install --upgrade pip
pip install -r requirements.txt
```

Generate the development database and run migrations.
```
python manage.py migrate
```

Generate compiled translation files.
```
python manage.py compilemessages --ignore venv
```

Populate the database with initial data. 
This will add a superuser with username `andryak` and password `alligatore1`.
```
python manage.py loaddata fixtures/initial.json
```

Run Redis to provide group capabilities to websockets.
```
./run_redis.sh
```

Run the development server.
```
python manage.py runserver
```

# Lint

The project linter is `pycodestyle` which is configured through the `setup.cfg` file. 

To run the linter on all relevant project files simply run `pycodestyle` from the directory containing the `setup.cfg` file.

# Localization

To update the localization strings for the application run:
```
python manage.py makemessages -l <locale> --ignore venv
```

This will update the `django.po` file stored in the `app/locale` folder adding missing strings. Edit this file providing 
appropriate translations and then run the following command to compile and serve them: 
```
python manage.py compilemessages --ignore venv
```