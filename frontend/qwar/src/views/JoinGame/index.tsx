import React, {useState} from 'react';
import {Button, View} from 'react-native';
import FixedLengthCodeInput from '../../components/FixedLengthCodeInput';
import {StackScreenProps} from '@react-navigation/stack';
import {RootStackParamList} from '../../routers';
import Title from '../../components/Title';
import {joinGame} from '../../api/game';
import ErrorMessage from '../../components/ErrorMessage';
import serverErrorToErrorMap from '../../utils/serverErrorToErrorMap';

interface IJoinGameProps
  extends StackScreenProps<RootStackParamList, 'JoinGame'> {}

interface IJoinGameFormErrors {
  form?: string;
}

const JoinGame: React.FC<IJoinGameProps> = ({navigation}) => {
  const [gameCode, setGameCode] = useState('');
  const [errors, setErrors] = useState<IJoinGameFormErrors>({});

  const handleJoinGame = () => {
    setErrors({});
    joinGame({gameCode})
      .then(() => {
        navigation.navigate('Lobby', {gameCode, creator: false});
      })
      .catch((error) => {
        setErrors(serverErrorToErrorMap(error));
      });
  };

  return (
    <View>
      <Title>Enter game code</Title>
      <FixedLengthCodeInput
        placeholder="Enter the game code here"
        onChangeText={(code) => setGameCode(code)}
        autoCapitalize="characters"
      />
      <Button title="Join game" onPress={handleJoinGame} disabled={!gameCode} />
      {errors.form && <ErrorMessage>{errors.form}</ErrorMessage>}
    </View>
  );
};

export default JoinGame;
