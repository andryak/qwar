import React, {useEffect, useState} from 'react';
import {StyleSheet, View} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Spinner from '../../components/Spinner';
import {useDispatch} from 'react-redux';
import {
  AUTO_LOGIN_FAILURE,
  AUTO_LOGIN_REQUEST,
  AUTO_LOGIN_SUCCESS,
} from '../../store/types/auth';
import {TOKEN_STORE_KEY, USERNAME_STORE_KEY} from '../../store/thunks/auth';
import {AuthAction} from '../../store/reducers/auth';
import {Dispatch} from 'redux';

const AutoLoginContainer: React.FC = ({children}) => {
  const dispatch = useDispatch<Dispatch<AuthAction>>();
  const [isAutoLoginInProgress, setIsAutoLoginInProgress] = useState(true);

  // Perform auto-login.
  useEffect(() => {
    const bootstrapAsync = async () => {
      dispatch({type: AUTO_LOGIN_REQUEST});

      let token: string | null;
      try {
        token = await AsyncStorage.getItem(TOKEN_STORE_KEY);
      } catch (error) {
        dispatch({type: AUTO_LOGIN_FAILURE});
        return;
      }

      let username: string | null;
      try {
        username = await AsyncStorage.getItem(USERNAME_STORE_KEY);
      } catch (error) {
        dispatch({type: AUTO_LOGIN_FAILURE});
        return;
      }

      if (token === null || username === null) {
        dispatch({type: AUTO_LOGIN_FAILURE});
        return;
      }

      dispatch({
        type: AUTO_LOGIN_SUCCESS,
        payload: {token, username},
      });
    };

    bootstrapAsync().finally(() => setIsAutoLoginInProgress(false));
  }, [dispatch]);

  if (isAutoLoginInProgress) {
    return (
      <View style={styles.spinnerContainer}>
        <Spinner />
      </View>
    );
  }

  return <>{children}</>;
};

const styles = StyleSheet.create({
  spinnerContainer: {flex: 1, alignItems: 'center', justifyContent: 'center'},
});

export default AutoLoginContainer;
