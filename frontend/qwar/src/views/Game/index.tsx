import React, {useEffect, useRef, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {StackScreenProps} from '@react-navigation/stack';
import {RootStackParamList} from '../../routers';
import {useSelector} from 'react-redux';
import {authSelector} from '../../store/selectors/auth';
import Spinner from '../../components/Spinner';
import {RNCamera} from 'react-native-camera';
import Sight from '../../components/Sight';
import Chat from '../../components/Chat';

interface IGameProps extends StackScreenProps<RootStackParamList, 'Game'> {}

const Game: React.FC<IGameProps> = ({route}) => {
  const {gameCode} = route.params;

  const webSocketRef = useRef<WebSocket>();
  const {token} = useSelector(authSelector);
  const [message, setMessage] = useState('');
  const [messages, setMessages] = useState<string[]>([]);

  useEffect(() => {
    const webSocket = new WebSocket(
      `ws://127.0.0.1:8000/ws/game/${gameCode}/?token=${token}`,
    );

    webSocket.onmessage = (e) => {
      const maxMessages = 10;
      const {event, data} = JSON.parse(e.data);
      if (event === 'message') {
        setMessages((prevMessages) =>
          [data, ...prevMessages].slice(0, maxMessages),
        );
      }
    };

    webSocketRef.current = webSocket;

    return () => {
      webSocket.close();
    };
  }, [gameCode, token]);

  const sendWebSocketMessage = (event: string, data: any) => {
    if (webSocketRef.current) {
      const webSocketMessage = JSON.stringify({event, data});
      webSocketRef.current.send(webSocketMessage);
    }
  };

  return (
    <View style={styles.container}>
      <RNCamera
        style={styles.camera}
        captureAudio={false}
        androidCameraPermissionOptions={{
          title: 'Permission to use camera',
          message: 'We need your permission to use your camera',
          buttonPositive: 'Ok',
          buttonNegative: 'Cancel',
        }}>
        {({status}) => {
          if (status === 'PENDING_AUTHORIZATION') {
            return <Spinner />;
          }
          if (status === 'NOT_AUTHORIZED') {
            return (
              <Text>
                The app needs access to the camera to start the game, please
                check your phone settings.
              </Text>
            );
          }
          return (
            <>
              <View style={styles.sightContainer}>
                <Sight color="white" opacity={0.75} width="90%" height="90%" />
              </View>
              <View style={styles.chatContainer}>
                <Chat
                  message={message}
                  messages={messages}
                  onChange={setMessage}
                  onSend={(text) => {
                    sendWebSocketMessage('message', text);
                    setMessage('');
                  }}
                />
              </View>
            </>
          );
        }}
      </RNCamera>
    </View>
  );
};

export default Game;

const styles = StyleSheet.create({
  container: {flex: 1},
  camera: {flex: 1},
  sightContainer: {flex: 0.5, justifyContent: 'center', alignItems: 'center'},
  chatContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    marginBottom: 22,
    marginHorizontal: 18,
  },
});
