import React, {useState} from 'react';
import {Button, TextInput, View} from 'react-native';
import ErrorMessage from '../../components/ErrorMessage';
import Title from '../../components/Title';
import {StackScreenProps} from '@react-navigation/stack';
import {RootStackParamList} from '../../routers';
import {useDispatch} from 'react-redux';
import {loginThunkCreator} from '../../store/thunks/auth';
import {ThunkDispatch} from 'redux-thunk';
import {IRootState} from '../../store/reducers';
import {AuthAction} from '../../store/reducers/auth';
import serverErrorToErrorMap from '../../utils/serverErrorToErrorMap';

interface ILoginProps extends StackScreenProps<RootStackParamList, 'Login'> {}

interface ILoginFormErrors {
  username?: string;
  password?: string;
  form?: string;
}

const Login: React.FC<ILoginProps> = ({navigation}) => {
  const dispatch = useDispatch<ThunkDispatch<IRootState, {}, AuthAction>>();

  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [errors, setErrors] = useState<ILoginFormErrors>({});

  const handleLogin = () => {
    setErrors({});
    setIsSubmitting(true);
    dispatch(loginThunkCreator({username, password}))
      .catch((error) => {
        setErrors(serverErrorToErrorMap(error));
      })
      .finally(() => {
        setIsSubmitting(false);
      });
  };

  return (
    <View>
      <Title>Welcome!</Title>
      <TextInput
        placeholder="Username"
        value={username}
        onChangeText={setUsername}
        autoCapitalize="none"
      />
      {errors.username && <ErrorMessage>{errors.username}</ErrorMessage>}
      <TextInput
        placeholder="Password"
        value={password}
        onChangeText={setPassword}
        secureTextEntry
      />
      {errors.password && <ErrorMessage>{errors.password}</ErrorMessage>}
      <Button
        title="Login"
        onPress={handleLogin}
        disabled={!username || !password || isSubmitting}
      />
      <Button
        title="Register"
        onPress={() => navigation.navigate('Signup')}
        disabled={isSubmitting}
      />
      {errors.form && <ErrorMessage>{errors.form}</ErrorMessage>}
    </View>
  );
};

export default Login;
