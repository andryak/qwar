import React from 'react';
import {StackScreenProps} from '@react-navigation/stack';
import {RootStackParamList} from '../../routers';
import Title from '../../components/Title';
import {Button, View} from 'react-native';

interface ILobbyProps extends StackScreenProps<RootStackParamList, 'Lobby'> {}

const Lobby: React.FC<ILobbyProps> = ({navigation, route}) => {
  const gameCode = route.params.gameCode;
  const creator = route.params.creator;
  return (
    <View>
      <Title>Lobby {gameCode}</Title>
      {creator && (
        <Button
          onPress={() => {
            navigation.navigate('Game', {gameCode});
          }}
          title="Start game"
        />
      )}
    </View>
  );
};

export default Lobby;
