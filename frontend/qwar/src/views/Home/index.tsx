import React, {useState} from 'react';
import {StackScreenProps} from '@react-navigation/stack';
import {RootStackParamList} from '../../routers';
import {Button, View} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {ThunkDispatch} from 'redux-thunk';
import {IRootState} from '../../store/reducers';
import {AuthAction} from '../../store/reducers/auth';
import {logoutThunkCreator} from '../../store/thunks/auth';
import {authSelector} from '../../store/selectors/auth';
import Title from '../../components/Title';
import ShareableQRCode from '../../components/ShareableQRCode';
import {createGame} from '../../api/game';

interface IHomeProps extends StackScreenProps<RootStackParamList, 'Home'> {}

const Home: React.FC<IHomeProps> = ({navigation}) => {
  const dispatch = useDispatch<ThunkDispatch<IRootState, {}, AuthAction>>();
  const {username} = useSelector(authSelector);
  const [isCreatingGame, setIsCreatingGame] = useState(false);

  const handleLogout = () => {
    dispatch(logoutThunkCreator());
  };

  const handleCreateGame = () => {
    setIsCreatingGame(true);
    createGame()
      .then((gameCode) => {
        navigation.navigate('Lobby', {gameCode, creator: true});
      })
      .finally(() => setIsCreatingGame(false));
  };

  const handleJoinGame = () => navigation.navigate('JoinGame');

  if (!username) {
    return null;
  }

  return (
    <View>
      <Title>Welcome {username}</Title>
      <ShareableQRCode value={username} />
      <Button onPress={handleLogout} title="Logout" disabled={isCreatingGame} />
      <Button
        onPress={handleCreateGame}
        title="Create game"
        disabled={isCreatingGame}
      />
      <Button
        onPress={handleJoinGame}
        title="Join game"
        disabled={isCreatingGame}
      />
    </View>
  );
};

export default Home;
