import axios from 'axios';
// @ts-ignore
import currentLocale from '../utils/currentLocale';
import store from '../store';
import {authSelector} from '../store/selectors/auth';

const client = axios.create({
  baseURL: 'http://localhost:8000/',
});

// Locale injector interceptor.
client.interceptors.request.use(
  (config) => ({
    ...config,
    headers: {
      'Accept-Language': currentLocale(),
      ...config.headers,
    },
  }),
  (error) => error,
);

// Auth token injector interceptor.
client.interceptors.request.use(
  (config) => {
    const state = store.getState();
    const {token} = authSelector(state);

    if (token === null) {
      return config;
    }

    return {
      ...config,
      headers: {
        Authorization: `Token ${token}`,
        ...config.headers,
      },
    };
  },
  (error) => error,
);

export default client;
