export interface ICredentials {
  username: string;
  password: string;
}

export type Token = string;

export type GameCode = string;

export interface IGameCodeWrapper {
  gameCode: GameCode;
}
