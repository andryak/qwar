import client from '../client';
import {GameCode, IGameCodeWrapper} from './types';

export function createGame(): Promise<GameCode> {
  return client.post<{code: GameCode}>('game/').then(({data}) => data.code);
}

export function joinGame({gameCode}: IGameCodeWrapper): Promise<void> {
  return client.post(`game/${gameCode}/player/`);
}
