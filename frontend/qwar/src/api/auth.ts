import {ICredentials, Token} from './types';
import client from '../client';

export function login({username, password}: ICredentials): Promise<Token> {
  return client
    .post<{token: Token}>('login/', {
      username,
      password,
    })
    .then(({data}) => data.token);
}

export function signup({username, password}: ICredentials): Promise<Token> {
  return client
    .post<{token: Token}>('signup/', {
      username,
      password,
    })
    .then(({data}) => data.token);
}
