import {NativeModules} from 'react-native';

function currentLocale(): string {
  const settings = NativeModules.SettingsManager.settings;
  // On iOS 13+ use the AppleLanguages array.
  if (
    Array.isArray(settings.AppleLanguages) &&
    settings.AppleLanguages.length > 0
  ) {
    return settings.AppleLanguages[0];
  }
  return settings.AppleLocale;
}

export default currentLocale;
