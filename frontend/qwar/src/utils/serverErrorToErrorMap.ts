import {AxiosError} from 'axios';

type ServerErrorMap = {[field: string]: string[]};
type ErrorMap = {[field: string]: string | undefined};

function serverErrorToErrorMap(error: AxiosError<ServerErrorMap>): ErrorMap {
  if (error.message === 'Network Error') {
    return {form: 'Cannot connect to the server'};
  }

  const headers = error.response?.headers;
  const data = error.response?.data;
  if (headers?.['content-type'] !== 'application/json' || !data) {
    return {form: 'Something went wrong'};
  }

  const {non_field_errors: nonFieldErrors, ...fieldErrors} = data;
  const mappedFieldErrors: ErrorMap = {};
  Object.entries(fieldErrors).forEach(([field, errors]) => {
    mappedFieldErrors[field] = errors?.[0];
  });
  return {
    ...mappedFieldErrors,
    form: nonFieldErrors?.[0],
  };
}

export default serverErrorToErrorMap;
