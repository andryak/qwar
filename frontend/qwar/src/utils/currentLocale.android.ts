import {NativeModules} from 'react-native';

function currentLocale(): string {
  return NativeModules.I18nManager.localeIdentifier;
}

export default currentLocale;
