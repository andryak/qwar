import {IAuthState} from '../reducers/auth';
import {IRootState} from '../reducers';

export const authSelector = (state: IRootState): IAuthState => state.auth;

export const isLoggedInSelector = (state: IRootState): boolean => {
  const {token} = authSelector(state);
  return token !== null;
};
