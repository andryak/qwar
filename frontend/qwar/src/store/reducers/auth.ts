import {
  AUTO_LOGIN_FAILURE,
  AUTO_LOGIN_REQUEST,
  AUTO_LOGIN_SUCCESS,
  LOGIN,
  LOGOUT,
} from '../types/auth';
import {Token} from '../../api/types';
import {Reducer} from 'redux';

export interface IAuthState {
  token: string | null;
  username: string | null;
}

export interface IAuthInfo {
  token: Token;
  username: string;
}

export type AuthAction =
  | {type: typeof AUTO_LOGIN_REQUEST}
  | {type: typeof AUTO_LOGIN_SUCCESS; payload: IAuthInfo}
  | {type: typeof AUTO_LOGIN_FAILURE}
  | {type: typeof LOGIN; payload: IAuthInfo}
  | {type: typeof LOGOUT};

const initialState: IAuthState = {
  token: null,
  username: null,
};

const authReducer: Reducer<IAuthState, AuthAction> = (
  state = initialState,
  action,
) => {
  switch (action.type) {
    case AUTO_LOGIN_REQUEST:
      return {
        token: null,
        username: null,
      };
    case AUTO_LOGIN_SUCCESS:
      return {
        token: action.payload.token,
        username: action.payload.username,
      };
    case AUTO_LOGIN_FAILURE:
      return {
        token: null,
        username: null,
      };
    case LOGIN:
      return {
        token: action.payload.token,
        username: action.payload.username,
      };
    case LOGOUT:
      return {
        token: null,
        username: null,
      };
    default:
      return state;
  }
};

export default authReducer;
