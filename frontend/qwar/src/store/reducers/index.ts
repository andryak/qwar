import authReducer, {IAuthState} from './auth';
import {combineReducers} from 'redux';

export interface IRootState {
  auth: IAuthState;
}

const rootReducer = combineReducers({
  auth: authReducer,
});

export default rootReducer;
