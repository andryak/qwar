import {ICredentials, Token} from '../../api/types';
import {login, signup as signupApi} from '../../api/auth';
import AsyncStorage from '@react-native-community/async-storage';
import {LOGIN, LOGOUT} from '../types/auth';
import {AuthAction} from '../reducers/auth';
import {ThunkAction} from 'redux-thunk';
import {IRootState} from '../reducers';

export const TOKEN_STORE_KEY = 'user/token';
export const USERNAME_STORE_KEY = 'user/username';

export const loginThunkCreator = (
  credentials: ICredentials,
): ThunkAction<Promise<Token>, IRootState, {}, AuthAction> => async (
  dispatch,
) => {
  const token = await login(credentials);
  await AsyncStorage.setItem(USERNAME_STORE_KEY, credentials.username);
  await AsyncStorage.setItem(TOKEN_STORE_KEY, token);
  dispatch({
    type: LOGIN,
    payload: {token, username: credentials.username},
  });
  return token;
};

export const signupThunkCreator = (
  credentials: ICredentials,
): ThunkAction<Promise<Token>, IRootState, {}, AuthAction> => async (
  dispatch,
) => {
  const token = await signupApi(credentials);
  await AsyncStorage.setItem(USERNAME_STORE_KEY, credentials.username);
  await AsyncStorage.setItem(TOKEN_STORE_KEY, token);
  dispatch({
    type: LOGIN,
    payload: {token, username: credentials.username},
  });
  return token;
};

export const logoutThunkCreator = (): ThunkAction<
  void,
  IRootState,
  {},
  AuthAction
> => async (dispatch) => {
  await AsyncStorage.removeItem(USERNAME_STORE_KEY);
  await AsyncStorage.removeItem(TOKEN_STORE_KEY);
  dispatch({type: LOGOUT});
};
