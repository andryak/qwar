import {createStore, applyMiddleware} from 'redux';
import rootReducer from './reducers';
import createDebugger from 'redux-flipper';
import thunk from 'redux-thunk';

const middlewares: any[] = [thunk];

if (__DEV__) {
  middlewares.push(createDebugger());
}

const store = createStore(rootReducer, applyMiddleware(...middlewares));

export default store;
