export const AUTO_LOGIN_REQUEST = 'auth/AUTO_LOGIN_REQUEST';
export const AUTO_LOGIN_SUCCESS = 'auth/AUTO_LOGIN_SUCCESS';
export const AUTO_LOGIN_FAILURE = 'auth/AUTO_LOGIN_FAILURE';
export const LOGIN = 'auth/LOGIN';
export const LOGOUT = 'auth/LOGOUT';
