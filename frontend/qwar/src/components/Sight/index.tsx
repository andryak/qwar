import React from 'react';
import Svg, {Circle, Color, Line, NumberProp, SvgProps} from 'react-native-svg';

interface ISightProps {
  width: NumberProp;
  height: NumberProp;
  color: Color;
}

const inherit = 'currentColor';

const Sight: React.FC<SvgProps> = (props) => (
  <Svg viewBox="0 0 100 100" {...props}>
    {/* Horizontal lines */}
    <Line x1={0} y1={50} x2={100} y2={50} strokeWidth={0.5} stroke={inherit} />
    <Line x1={0} y1={50} x2={30} y2={50} strokeWidth={2} stroke={inherit} />
    <Line x1={70} y1={50} x2={100} y2={50} strokeWidth={2} stroke={inherit} />
    <Line
      x1={42.5}
      y1={50}
      x2={47.5}
      y2={50}
      strokeWidth={2}
      stroke={inherit}
    />
    <Line
      x1={52.5}
      y1={50}
      x2={57.5}
      y2={50}
      strokeWidth={2}
      stroke={inherit}
    />

    {/* Vertical lines */}
    <Line x1={50} y1={10} x2={50} y2={90} strokeWidth={0.5} stroke={inherit} />
    <Line x1={50} y1={10} x2={50} y2={30} strokeWidth={2} stroke={inherit} />
    <Line x1={50} y1={70} x2={50} y2={90} strokeWidth={2} stroke={inherit} />
    <Line
      x1={50}
      y1={42.5}
      x2={50}
      y2={47.5}
      strokeWidth={2}
      stroke={inherit}
    />
    <Line
      x1={50}
      y1={52.5}
      x2={50}
      y2={57.5}
      strokeWidth={2}
      stroke={inherit}
    />

    <Circle cx={50} cy={50} r={22.5} strokeWidth={0.5} stroke={inherit} />
    <Circle cx={50} cy={50} r={30} strokeWidth={1} stroke={inherit} />
  </Svg>
);

export default Sight;
