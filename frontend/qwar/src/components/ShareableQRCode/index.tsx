import QRCode, {QRCodeProps} from 'react-native-qrcode-svg';
import React, {useRef} from 'react';
import {Share, TouchableOpacity} from 'react-native';

const ShareableQRCode: React.FC<QRCodeProps> = (props) => {
  const qrCodeRef = useRef<any>();

  const shareQRCode = () => {
    if (qrCodeRef.current) {
      qrCodeRef.current.toDataURL((dataUrl: string) => {
        // FIXME This crashes on a simulated iPhone 11 Pro Max (13.6) when the
        //  user selects 'Save to Files' and then closes both dialogs.
        //  This looks like a bug in `Share`.
        Share.share({
          url: `data:image/png;base64,${dataUrl}`,
        });
      });
    }
  };

  return (
    <TouchableOpacity onPress={shareQRCode}>
      <QRCode
        {...props}
        getRef={(ref) => {
          qrCodeRef.current = ref;
        }}
      />
    </TouchableOpacity>
  );
};

export default ShareableQRCode;
