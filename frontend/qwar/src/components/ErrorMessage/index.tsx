import React from 'react';
import {StyleSheet, Text, TextProps} from 'react-native';

const ErrorMessage: React.FC<TextProps> = ({children, style, ...props}) => (
  <Text style={[styles.container, style]} {...props}>
    {children}
  </Text>
);

export default ErrorMessage;

const styles = StyleSheet.create({
  container: {},
});
