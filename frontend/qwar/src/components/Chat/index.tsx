import {
  Button,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';
import React from 'react';

interface IChatProps {
  message: string;
  messages: string[];
  onChange: (message: string) => void;
  onSend: (message: string) => void;
}

const Chat: React.FC<IChatProps> = ({message, messages, onChange, onSend}) => {
  return (
    <View style={styles.container}>
      <ScrollView style={styles.messagesContainer}>
        {messages.map((msg, index) => (
          <Text style={styles.message} key={index}>
            {msg}
          </Text>
        ))}
      </ScrollView>
      <View style={styles.inputAndButtonContainer}>
        <TextInput
          style={styles.textInput}
          placeholder={'Write a message here...'}
          value={message}
          onChangeText={onChange}
          autoCorrect={false}
          maxLength={120}
        />
        <View style={styles.buttonContainer}>
          <Button
            onPress={() => {
              onSend(message);
            }}
            title="Send"
            disabled={!message}
          />
        </View>
      </View>
    </View>
  );
};

export default Chat;

const styles = StyleSheet.create({
  container: {justifyContent: 'flex-end'},
  messagesContainer: {},
  message: {color: 'white'},
  inputAndButtonContainer: {
    flexDirection: 'row',
    marginTop: 12,
    alignItems: 'center',
  },
  textInput: {
    color: 'white',
    padding: 12,
    borderWidth: 1,
    borderColor: 'white',
    borderRadius: 12,
    flex: 1,
  },
  buttonContainer: {marginLeft: 18},
});
