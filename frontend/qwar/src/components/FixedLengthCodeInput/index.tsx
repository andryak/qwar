import React from 'react';
import {StyleSheet, TextInput, TextInputProps} from 'react-native';

interface IFixedLengthCodeInputProps extends TextInputProps {}

const FixedLengthCodeInput: React.FC<IFixedLengthCodeInputProps> = ({
  style,
  ...props
}) => <TextInput style={[styles.container, style]} {...props} />;

export default FixedLengthCodeInput;

const styles = StyleSheet.create({
  container: {},
});
