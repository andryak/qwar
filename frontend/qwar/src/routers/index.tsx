import Login from '../views/Login';
import Signup from '../views/Signup';
import Home from '../views/Home';
import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {useSelector} from 'react-redux';
import {isLoggedInSelector} from '../store/selectors/auth';
import JoinGame from '../views/JoinGame';
import Lobby from '../views/Lobby';
import Game from '../views/Game';

export interface ILobbyParamList {
  gameCode: string;
  creator: boolean;
}

export interface IGameParamList {
  gameCode: string;
}

export type RootStackParamList = {
  Login: undefined;
  Signup: undefined;
  Home: undefined;
  Lobby: ILobbyParamList;
  JoinGame: undefined;
  Game: IGameParamList;
};

const Stack = createStackNavigator<RootStackParamList>();

const RootStackNavigator = () => {
  const isLoggedIn = useSelector(isLoggedInSelector);

  if (isLoggedIn) {
    return (
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="Lobby" component={Lobby} />
        <Stack.Screen name="JoinGame" component={JoinGame} />
        <Stack.Screen name="Game" component={Game} />
      </Stack.Navigator>
    );
  }

  return (
    <Stack.Navigator initialRouteName="Login">
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="Signup" component={Signup} />
    </Stack.Navigator>
  );
};

export default RootStackNavigator;
