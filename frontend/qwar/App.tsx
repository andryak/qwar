import React from 'react';
import {StatusBar} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import RootStackNavigator from './src/routers';
import AutoLoginContainer from './src/views/AutoLoginContainer';
import {Provider as ReduxStoreProvider} from 'react-redux';
import store from './src/store';

const App = () => {
  return (
    <ReduxStoreProvider store={store}>
      <AutoLoginContainer>
        <NavigationContainer>
          <StatusBar barStyle="dark-content" />
          <RootStackNavigator />
        </NavigationContainer>
      </AutoLoginContainer>
    </ReduxStoreProvider>
  );
};

export default App;
